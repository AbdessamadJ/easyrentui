import React from 'react';
import {Layout, Menu} from 'antd';
import './style.css';
import {Link, NavLink} from "react-router-dom";
import logo from '../../assets/easyrent-removebg-preview.png';

const {Header} = Layout;

const NavBar = () => {
    return (
        <Header className="nav-bar-header">
            <span className="logo">
                <Link to="/">
                    <img src={logo} alt="Easy Rent"/>
                </Link>
            </span>
            <Menu className="nav-bar-menu" theme="dark" mode="horizontal">
                <Menu.Item key="1">
                    <NavLink to="/">
                        <span>Home</span>
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="2">
                    <NavLink to="/register">
                        <span>Register</span>
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="3">
                    <NavLink to="/login">
                        <span>Login</span>
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="/help">
                    <NavLink to="/help">
                        <span>Help</span>
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="/about">
                    <NavLink to="/about">
                        <span>About</span>
                    </NavLink>
                </Menu.Item>
            </Menu>
        </Header>
    );
};

export default NavBar;