import React from "react";
import './style.css';
import ItemCard from "./ItemCard.jsx";

class ItemList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [
                {id: 1, name: 'A', description:'This is description', imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'},
                {id: 2, name: 'A', description:'This is description',imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'},
                {id: 3, name: 'A', description:'This is description', imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'},
                {id: 4, name: 'A', description:'This is description', imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'},
                {id: 5, name: 'A', description:'This is description', imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'},
            ]
        }
    }

    render() {
        return (
            <div className="item-list-div">
                {this.state.items.map(item => <ItemCard key={item.id} data={item}/>)}
            </div>
        )
    }
}

ItemList.propTypes = {}
export default ItemList;