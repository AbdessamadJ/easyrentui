import React from 'react';
import './style.css';
import {Card} from 'antd';
import {useHistory} from "react-router-dom";

const {Meta} = Card;

const ItemCard = (props) => {
    const history = useHistory();

    const onImageClick = () => {
        history.push('/view/'+props.data.id);
    };

    return (
        <Card
            className="item-card-div"
            hoverable
            cover={<img alt={props.data.name} src={props.data.imagePath} />}
            onClick={() => onImageClick()}
        >
            <Meta title={props.data.name} description= {props.data.description} />
        </Card>
    );
}

export default ItemCard;