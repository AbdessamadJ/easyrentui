import React from "react";
import './style.css';
import {Row} from "antd";
import ItemImage from "./ItemImage";
import ItemDescription from "./ItemDescription";

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {id: 1, name: 'A', description:'This is description', imagePath: 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'}
        }
    }

    render() {
        return (
            <Row>
                <ItemImage data={this.state.data}/>
                <ItemDescription data={this.state.data}/>
            </Row>
        )
    }
}

Index.propTypes = {}
export default Index;