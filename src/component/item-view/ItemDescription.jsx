import React, {useState} from 'react';
import {Button, Col, Descriptions} from "antd";
import { DatePicker, Space } from 'antd';

const { RangePicker } = DatePicker;

const ItemDescription = (props) => {
    const [displayRentOption, setDisplayRentOption] = useState(false);

    return (
        <Col md={12}>
            <div className="item-descriptions">
                <Descriptions title="Item Description"
                              layout="horizontal"
                              column={{ md: 1 }}
                              bordered
                              size='middle'
                >
                    <Descriptions.Item label="Name">{props.data.name}</Descriptions.Item>
                    <Descriptions.Item label="Description">{props.data.description}</Descriptions.Item>
                </Descriptions>

                <div className="rent-it-option-div">
                    <div className="rent-it-button">
                        <Button type="primary"
                                size={'large'}
                                disabled={displayRentOption}
                                onClick={() => setDisplayRentOption(!displayRentOption)}
                        >
                            Rent it
                        </Button>
                    </div>

                    {
                        displayRentOption ?
                            <div className='rent-it-option'>
                                <span><RangePicker /></span>
                                <span>
                                    <Button type="primary"
                                            size={'large'}
                                            disabled={!displayRentOption}
                                    >
                                    Submit Request
                                </Button>
                                </span>
                            </div>
                            : ''
                    }
                </div>
            </div>
        </Col>
    );
}

export default ItemDescription;