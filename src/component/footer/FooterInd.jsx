import React from 'react';
import './style.css';
import {Layout} from 'antd';

const {Footer} = Layout;

const FooterInd = () => {
    return (
        <Footer className="footer-div">
            EasyRent © 2021 Designed & Developed by Lazy Developers
        </Footer>
    );
}

export default FooterInd;