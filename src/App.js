import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./view/home/Home.jsx";
import './App.css';
import Register from "./view/register/Register";
import Login from "./view/login/Login";
import Item from "./view/item/Item";

function App() {
  return (
      <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <Route path = "/view/:id" component={Item}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
