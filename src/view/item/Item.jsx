import React from "react";
import './style.css';
import NavBar from "../../component/navbar/NavBar";
import FooterInd from "../../component/footer/FooterInd";
import {Layout} from "antd";
import Index from "../../component/item-view/Index";

const { Content } = Layout;

class Item extends React.Component {
    render() {
        return (
            <Layout>
                <NavBar/>

                <Content className="main-body">
                    <Index/>
                </Content>
                <FooterInd/>
            </Layout>
        )
    }
}

Item.propTypes = {}
export default Item;